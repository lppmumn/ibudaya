<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@getHome');
Route::get('/home', 'PagesController@getHome');

Route::get('/about', 'PagesController@getAbout');

Route::get('/contact', 'PagesController@getContact');

Route::get('/display/create', 'ItemController@create')->name('display.create');
Route::post('/display/create', 'ItemController@store')->name('display.store');
Route::get('/display','ItemController@DisplayItems')->name('display.display-items');
Route::get('/display/{id}', 'ItemController@detailItem')->name('display.detail-item');

Route::get('/messages', 'MessagesController@getMessages');

Route::post('/contact/submit', 'MessagesController@submit');

Auth::routes();



Route::post('/search','PagesController@searchItem');