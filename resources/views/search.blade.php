@extends('layouts.app')

@section('content')

<div class="container">
    @if(isset($items))
        @if(session()->has('warning'))
            {!! session()->get('warning') !!}
        @endif
        <p> The Search results for your query '<b> {{ $query }} </b>' are :</p>
        <table id="table_search" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                <?php $count = 1; ?>
                @foreach($items as $item)
                <tr onclick="displayItem({{$item->id}})">
                    <td>{{$count}}</td>
                    <td>{{$item->title}}</td>
                    <td><img src="{{ url($item->filename) }}" alt="{{$item->title}}" title="{{$item->title}}" /></td>
                </tr>
                <?php $count++; ?>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
<script>
function displayItem(x) {
    window.location.href = "/display/"+x;
}
</script>
<style>
    table#table_search tbody  tr {
        cursor : pointer;
    } 
</style>
@endsection