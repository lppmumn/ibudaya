<nav class="navbar navbar-expand-md navbar-dark bg-dark">
<!--
  
    @include('inc.sidemenu')
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    
  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item {{Request::is('/') ? 'active' : ''}}">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item {{Request::is('about') ? 'active' : ''}}">
        <a class="nav-link" href="/about">About</a>
      </li>
      <li class="nav-item {{Request::is('contact') ? 'active' : ''}}">
        <a class="nav-link" href="/contact">Contact</a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
    <form class="navbar-form navbar-right" action="/action_page.php">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="search">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <i class="glyphicon glyphicon-search"></i>
          </button>
        </div>
      </div>
    </form>
  </div>
-->
    
    <div class="container">
<!--        @include('inc.sidemenu')-->
        <a class="navbar-brand navbar-left" href="/">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto" id="menu_id">
            <li class="nav-item {{Request::is('/') ? 'active' : ''}}">
                <a class="nav-link" href="/home"><span class="glyphicon glyphicon-home" style="font-size:25px"></span></a>
            </li>
            <li class="nav-item {{Request::is('display') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('display.display-items')}}"><span class="glyphicon glyphicon-list-alt" style="font-size:25px"></span></a>
            </li>
        <!-- Authentication Links -->
        @guest
            <li><a class="nav-link" href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in" style="font-size:25px"></span></a></li>
        @else
              
            <li class="nav-item {{Request::is('upload') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('display.create')}}"><span class="glyphicon glyphicon-upload" style="font-size:25px"></span></a>
            </li>
            <li class="nav-item">
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <span class="glyphicon glyphicon-log-out" style="font-size:25px"></span>
                </a>
            </li>
        @endguest
          </ul>
        </div>
        <form class="navbar-form navbar-right" action="/search" method="POST" role="search">
            {{ csrf_field() }}
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="q" required>
            <div class="input-group-btn">
              <button class="btn btn-default" type="submit">
                <i class="glyphicon glyphicon-search"></i>
              </button>
            </div>
          </div>
        </form>
    </div>
</nav>
<style>
    #menu_id > li{
        margin-left:10px;
    }
</style>
