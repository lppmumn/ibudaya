@extends('layouts.app')

@section('content')
<html>
   <body>
      <form id="uploadForm" action="{{ route('display.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        Name:
        <br />
        <input type="text" name="name" />
        <br /><br />
        Description:
        <br />
        <textarea form="uploadForm" rows="4" cols="50" name="description"></textarea>
        <br /><br />
        Image:
        <br />
        <input type="file" name="image" />
        <br /><br />
        <input type="submit" value="Upload" />
       </form>
   </body>
</html>
@endsection