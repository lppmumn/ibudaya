<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ config('app.name', 'iBudaya') }}</title>
        @include('inc.style')        
        @include('inc.script')
    </head>
    
    <body>
        @if(Request::is('/'))
            @include('inc.showcase')
        @else
        @include('inc.navbar') 
        <div class="container">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-8 col-lg-8">
                    @include('inc.alert')
                    @yield('content')
                </div>
                <div class="col-md-4 col-lg-4">
                    @if(!Request::is('search'))
                        @include('inc.sidebar')
                    @endif
                </div>
            </div>
        @endif
        </div>
    
<!--
        <footer class="footer">
          <div class="container">
            
            <div class="col-md-6">
                <h5 class="text-uppercase">Footer Content</h5>
                <p>Here you can use rows and columns here to organize your footer content.</p>
            </div>

            <div class="col-md-6">
                <h5 class="text-uppercase">Links</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Link 1</a>
                    </li>
                    <li>
                        <a href="#!">Link 2</a>
                    </li>
                    <li>
                        <a href="/about">About</a>
                    </li>
                    <li>
                        <a href="/contact">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="footer-copyright text-center">
                <span class="text-muted">Copyright 2018 &copy; iBudaya</span> 
            </div>
          </div>
        </footer>
-->
    </body>
</html>
<!--
    <style>
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: black;
        color: white;
        text-align: center;
    }
    </style>-->
