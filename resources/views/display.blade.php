@extends('layouts.app')

@section('content')
    <h1>Uploaded Files</h1>
    <div class="row text-center">

        @foreach ($items as $item)
            <div class="col-lg-3 col-md-6 mb-4">
              <div class="card">
                <a href="{{ route('display.detail-item',$item->id)}}">
                    <img class="card-img-top" src="{{asset('storage/'.$item->filename)}}" alt="{{$item->title}}">
                </a>
                <div class="card-body">
                  <h4 class="card-title">{{$item->title}}</h4>
                  <p class="card-text">{{$item->description}}</p>
                </div>
              </div>
            </div>
        @endforeach 

    </div>
@endsection