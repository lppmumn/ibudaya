@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
<script>
function like(x) {
    x.classList.toggle("fa-thumbs-up");
}
    
function dislike(x) {
    x.classList.toggle("fa-thumbs-down");
}
</script>
</head>
<body>
    <div class="panel panel-default">
        <div class="panel-body"><img src="{{asset('storage/'.$items->filename)}}" alt="image" width="100%" height="100%"/></div>
        <div class="panel-body" style="border-bottom: 1px solid black;">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="title_text"></div>
                    <strong>{{$items->title}}</strong>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-xs-7 col-sm-7 col-md-7">
                    2 views
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                   <i class="fa fa-thumbs-o-up" onclick="like(this)" style="font-size: 30px; cursor: pointer;"></i>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                   <i class="fa fa-thumbs-o-down" onclick="dislike(this)" style="font-size: 30px; cursor: pointer;"></i>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <i class="material-icons" style="font-size:30px; cursor: pointer;" data-toggle="modal" data-target="#myModal" >share</i>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <i class="material-icons" style="font-size:30px">settings</i>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1">
                    <i href= "" style="font-size:30px" class="material-icons">report</i>
                </div>
            </div> -->
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 col-sm-2 col-md-2">
                    <img class="img-circle" src="\img\avatar.png" style="width: 50px; height: 50px"/>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="row">
                        <strong>{{$user->name}}</strong>
                    </div>
                    
                    <div class="row">
                        Published on {{$items->updated_at}}
                    </div>
                    
                    <div class="row" style="margin-top: 30px">
                        {{$items->description}}
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal" id="myModal" role="dialog">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                    <h3>Share</h3>
                </div>
                <div class="modal-body">
                    <div style="text-align:center;">
                        <ul class="list-inline social-buttons">
                            <li class="list-inline-item">
                              <a href="https://twitter.com/share?url={{Request::url()}};text=Check%20this%20out&amp;hashtags=asdasdasd" target="_blank">
                                <i class="fa fa-twitter"></i>
                              </a>
                            </li>
                            <li class="list-inline-item">
                              <a href="http://www.facebook.com/sharer.php?u={{Request::url()}}" target="_blank">
                                <i class="fa fa-facebook"></i>
                              </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
