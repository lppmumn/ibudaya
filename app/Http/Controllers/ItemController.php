<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\items;
use Storage;
use App\User;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('uploadFile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        
        $name = $request->input('name');
        $description = $request->input('description');
        $image = $request->file('image');
        $destination = public_path('images/');
        $filename = str_random(30).'.'.$image->extension();
        $folderLocation = 'image/';
       // $exists = Storage::disk('public')->exists('image/thumb');
       // if(!$exists){
       //     Storage::disk('public')->makeDirectory('image/thumb',0777,true);
       // }
       Storage::disk('public')->putFileAs("image",$image,$filename);
        
        
        items::create([
                'title' => $name,
                'description' => $description,
                'filename' => $folderLocation.$filename,
                'user_id' => $user->id
        ]);
        
//        $source = Storage::disk('local')->url($filename);
        //$img = Image::make($image->getRealPath())->save('storage/image/thumb/'.$name);
        
        return redirect('/home')->with('status_success','Uploading Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function displayItems()
    {
        $items = items::all();
        
        return view('display',compact('items'));
    }

    public function detailItem($id)
    {
        //query select ke table 'products_photos'
        $items = items::find($id);
        $user_id = $items->user_id;
        $user = User::find($user_id);
        return view('display_item',compact('items','user')); 
    }

}
