<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DB;
use App\items;
use App\User;
class PagesController extends Controller
{
    public function getHome(){
        return view('home');
    }
    
    public function getAbout(){
        return view('about');
    }
    
    public function getContact(){
        return view('contact');
    }
    
    public function getDisplay(){
        return view('display');
    }
    
    public function searchItem(Request $request)
    {
        $q = $request->input('q');
        $item = items::where('title','LIKE','%'.$q.'%')->get();
        if(count($item) > 0)
            return view('search')->withItems($item)->withQuery ($q);
        else return view('search')->withErrors('NO ITEMS FOUND.');
    }
}
