<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class items extends Model
{
    protected $fillable = ['title','description','filename', 'user_id'];
    protected $table = 'items';
}
